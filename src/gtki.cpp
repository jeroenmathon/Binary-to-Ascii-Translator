//
// Created by jeroen on 9/17/15.
//

#include "gtki.h"

gtkInterface::gtkInterface()
{

}

gtkInterface::~gtkInterface()
{

}

void gtkInterface::binaryButtonOnClick()
{
    Glib::RefPtr<Gtk::TextBuffer> asciiBuffer = asciiTextView->get_buffer();
    Glib::RefPtr<Gtk::TextBuffer> binaryBuffer = binaryTextView->get_buffer();

    binaryBuffer->set_text(translate.asciiToBinary(asciiBuffer->get_text()));
}

void gtkInterface::asciiButtonOnClick()
{
    Glib::RefPtr<Gtk::TextBuffer> asciiBuffer = asciiTextView->get_buffer();
    Glib::RefPtr<Gtk::TextBuffer> binaryBuffer = binaryTextView->get_buffer();
    asciiBuffer->set_text(translate.binaryToAscii(binaryBuffer->get_text()).c_str());
}

void gtkInterface::init()
{
    builder->get_widget("window1", window);
    builder->get_widget("asciiScrollWindow", asciiScrollWindow);
    builder->get_widget("binaryScrollWindow", binaryScrollWindow);
    builder->get_widget("mainBox", mainBox);
    builder->get_widget("buttonBox", buttonBox);
    builder->get_widget("translateBox", translateBox);
    builder->get_widget("textViewLabelBox", textViewLabelBox);
    builder->get_widget("asciiTextView", asciiTextView);
    builder->get_widget("binaryTextView", binaryTextView);
    builder->get_widget("binaryTranslateButton", binaryTranslateButton);
    builder->get_widget("asciiTranslateButton", asciiTranslateButton);
    builder->get_widget("mainLabel", mainLabel);
    builder->get_widget("asciiLabel", asciiLabel);
    builder->get_widget("binaryLabel", binaryLabel);
    builder->get_widget("seperator", seperator);
    builder->get_widget("textViewLabelSeperator", textViewLabelSeperator);

    binaryTranslateButton->signal_pressed().connect(sigc::mem_fun(this, &gtkInterface::binaryButtonOnClick));
    asciiTranslateButton->signal_clicked().connect(sigc::mem_fun(this, &gtkInterface::asciiButtonOnClick));

    window->show();
}

void gtkInterface::run()
{

}