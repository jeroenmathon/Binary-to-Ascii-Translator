//
// Created by jeroen on 9/17/15.
//

#include "translate.h"

translator::translator()
{

}

translator::~translator()
{

}

std::string translator::binaryToAscii(std::string binary)
{
    std::string output;
    std::stringstream binaryStream(binary);

    while(binaryStream.good())
    {
        std::bitset<8> bits;
        binaryStream >> bits;
        output += char(bits.to_ulong());
    }

    return output;
}

std::string translator::asciiToBinary(std::string ascii)
{
    std::stringstream output;
    for(std::size_t i(0);i < ascii.size(); i++)
    {
        output <<  std::bitset<8>(ascii.c_str()[i]);
    }
    return output.str();
}