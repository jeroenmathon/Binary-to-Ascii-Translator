//
// Created by jeroen on 9/17/15.
//

#ifndef BINARY_TO_ASCII_TRANSLATOR_GTKI_H
#define BINARY_TO_ASCII_TRANSLATOR_GTKI_H

#include <iostream>
#include <gtkmm.h>
#include "translate.h"

class gtkInterface
{
public:
    // Constructor and Destructor
    gtkInterface();
    ~gtkInterface();

    // Public routines
    void init();
    void run();

    // Public variables
    Gtk::Window *window;

private:
    // Private routines
    void asciiButtonOnClick();
    void binaryButtonOnClick();

    // Private variables
    translator translate;
    Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create_from_file("ui/ui.glade");
    Gtk::Box *mainBox;
    Gtk::Box *translateBox;
    Gtk::Box *buttonBox;
    Gtk::Box *textViewLabelBox;
    Gtk::Label *mainLabel;
    Gtk::Label *asciiLabel;
    Gtk::Label *binaryLabel;
    Gtk::TextView *asciiTextView;
    Gtk::TextView *binaryTextView;
    Gtk::Button *asciiTranslateButton;
    Gtk::Button *binaryTranslateButton;
    Gtk::Separator *seperator;
    Gtk::Separator *textViewLabelSeperator;
    Gtk::ScrolledWindow *asciiScrollWindow;
    Gtk::ScrolledWindow *binaryScrollWindow;
};
#endif //BINARY_TO_ASCII_TRANSLATOR_GTKI_H
