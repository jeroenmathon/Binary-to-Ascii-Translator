//
// Created by jeroen on 9/17/15.
//

#ifndef BINARY_TO_ASCII_TRANSLATOR_TRANSLATE_H
#define BINARY_TO_ASCII_TRANSLATOR_TRANSLATE_H

#include <iostream>
#include <sstream>
#include <bitset>

class translator
{
public:
    // Constructor and Destructor
    translator();
    ~translator();

    // Public routines
    std::string asciiToBinary(std::string ascii);
    std::string binaryToAscii(std::string binary);
};
#endif //BINARY_TO_ASCII_TRANSLATOR_TRANSLATE_H
