#include <gtkmm.h>
#include "gtki.h"

int main(int argc, char **argv)
{
    Glib::RefPtr<Gtk::Application> app =
            Gtk::Application::create(argc, argv, "Binary to Ascii Translator");

    gtkInterface gInt;
    gInt.init();

    return app->run(*gInt.window);
}